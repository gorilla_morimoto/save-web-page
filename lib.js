// @ts-check

const fs = require('fs');
const { sep } = require('path');

/** @param {string} url */
/** @return {string} */
const trimProto = url => url.replace(/https?:\/\//, '');

/** @param {string} url*/
/** @return {string} */
const getHost = url => trimProto(url).split('/', 1)[0];

const paths = Object.freeze({
  pdf: 'main.pdf',
  meta: 'meta.json',
  thum: 'thum.png',
  fullPage: 'fullPage.png'
});

/** @param {string} url*/
/** @return {object} */
const initDir = url => {
  const host = getHost(url);
  const hostDir = `pages${sep}${host}`;
  if (!fs.existsSync('pages')) {
    fs.mkdirSync('pages');
  }
  if (!fs.existsSync(hostDir)) {
    fs.mkdirSync(hostDir);
  }
};

module.exports = { initDir, getHost, paths };
